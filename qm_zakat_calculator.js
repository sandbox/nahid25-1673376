
jQuery(document).ready(function() {

  // add the prev & next buttons
  jQuery('#qm-zakat-pagination-2').append("<br/><input type='button' value='Prev' class='zakat-prev-Btn'>");
  jQuery('#qm-zakat-pagination-3').append("<br/><input type='button' value='Prev' class='zakat-prev-Btn'>");
  jQuery('#qm-zakat-pagination-4').append("<br/><input type='button' value='Prev' class='zakat-prev-Btn'>");

  jQuery('#qm-zakat-pagination-1').append("<input type='button' value='Next' class='zakat-next-Btn'>");
  jQuery('#qm-zakat-pagination-2').append("<input type='button' value='Next' class='zakat-next-Btn'>");
  jQuery('#qm-zakat-pagination-3').append("<input type='button' value='Next' class='zakat-next-Btn'>");

  // as display:none in css so show the first page
  jQuery('.qm-zakat-pagination:first').show();

  // pagination
  jQuery('.zakat-prev-Btn').click(function(){

    jQuery(this).parent().hide();
    jQuery(this).parent().prev().show();

  });

  jQuery('.zakat-next-Btn').click(function(){

    jQuery(this).parent().hide();
    jQuery(this).parent().next().show();

  });



  jQuery('#qm-zakat-amount-1, #qm-zakat-amount-2, #qm-zakat-amount-3, #qm-zakat-amount-4, #qm-zakat-amount-5, #qm-zakat-amount-6, #qm-zakat-amount-7, #qm-zakat-amount-8, #qm-zakat-amount-9, #qm-zakat-amount-10, #qm-zakat-amount-11, #qm-zakat-amount-12, #qm-zakat-amount-13, #qm-zakat-amount-14, #qm-zakat-amount-15, #qm-zakat-amount-16, #qm-zakat-amount-17, #qm-zakat-amount-18, #qm-zakat-amount-19, #qm-zakat-amount-20, #qm-zakat-amount-21,  #qm-zakat-amount-22, #qm-zakat-amount-23, #qm-zakat-neg-amount-1, #qm-zakat-neg-amount-2, #qm-zakat-neg-amount-3').change(function() {

    var total = parseInt(jQuery("#qm-zakat-amount-1").val()) +  parseInt(jQuery("#qm-zakat-amount-2").val()) + parseInt(jQuery("#qm-zakat-amount-3").val()) + parseInt(jQuery("#qm-zakat-amount-4").val()) + parseInt(jQuery("#qm-zakat-amount-5").val()) + parseInt(jQuery("#qm-zakat-amount-6").val()) + parseInt(jQuery("#qm-zakat-amount-7").val()) + parseInt(jQuery("#qm-zakat-amount-8").val()) + parseInt(jQuery("#qm-zakat-amount-9").val()) + parseInt(jQuery("#qm-zakat-amount-10").val()) + parseInt(jQuery("#qm-zakat-amount-11").val()) + parseInt(jQuery("#qm-zakat-amount-12").val()) + parseInt(jQuery("#qm-zakat-amount-13").val()) + parseInt(jQuery("#qm-zakat-amount-14").val()) + parseInt(jQuery("#qm-zakat-amount-15").val()) + parseInt(jQuery("#qm-zakat-amount-16").val()) + parseInt(jQuery("#qm-zakat-amount-17").val()) + parseInt(jQuery("#qm-zakat-amount-18").val()) + parseInt(jQuery("#qm-zakat-amount-19").val()) + parseInt(jQuery("#qm-zakat-amount-20").val()) + parseInt(jQuery("#qm-zakat-amount-21").val()) + parseInt(jQuery("#qm-zakat-amount-22").val()) + parseInt(jQuery("#qm-zakat-amount-23").val())
      - parseInt(jQuery("#qm-zakat-neg-amount-1").val()) - parseInt(jQuery("#qm-zakat-neg-amount-2").val()) - parseInt(jQuery("#qm-zakat-neg-amount-3").val()) ;

    var nisab = jQuery("#nisab").val();

    if (total < nisab) {
      var zakat = 0;
    }else{
      var zakat = total;
    }
    // results
    if (!isNaN(zakat)) {
      jQuery("#qm-total-amount").html(total);
      jQuery("#qm-zakat-amount").html(zakat * 0.025);
    }
  });

});

