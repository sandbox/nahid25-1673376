<?php

function qm_zakat_nisab_form($form, &$form_state) {

  $form['zakat_nisab_amount'] = array(
    '#title' => t('Set the minimum/ nisab amount:'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('zakat_nisab_amount', 75000),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

function qm_zakat_nisab_form_submit($form, &$form_state){

  $result_nisab = $form_state['values']['zakat_nisab_amount'];
  variable_set('zakat_nisab_amount', $result_nisab);

  drupal_set_message("Submitted");

}
